package com.thetransactioncompany.jsonrpc2;


import net.minidev.json.JSONAware;
import net.minidev.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The base abstract class for JSON-RPC 2.0 requests, notifications and
 * responses. Provides common methods for parsing (from JSON string) and
 * serialisation (to JSON string) of the three message types.
 *
 * <p>Example parsing and serialisation back to JSON:
 *
 * <pre>
 * String jsonString = "{\"method\":\"progressNotify\",\"params\":[\"75%\"],\"jsonrpc\":\"2.0\"}";
 *
 * JSONRPC2Message message = null;
 *
 * // parse
 * try {
 *        message = JSONRPC2Message.parse(jsonString);
 * } catch (JSONRPC2ParseException e) {
 *        // handle parse exception
 * }
 *
 * if (message instanceof JSONRPC2Request)
 *        System.out.println("The message is a request");
 * else if (message instanceof JSONRPC2Notification)
 *        System.out.println("The message is a notification");
 * else if (message instanceof JSONRPC2Response)
 *        System.out.println("The message is a response");
 *
 * // serialise back to JSON string
 * System.out.println(message);
 *
 * </pre>
 *
 * <p id="map">The mapping between JSON and Java entities (as defined by the 
 * underlying JSON Smart library): 
 *
 * <pre>
 *     true|false  &lt;---&gt;  java.lang.Boolean
 *     number      &lt;---&gt;  java.lang.Number
 *     string      &lt;---&gt;  java.lang.String
 *     array       &lt;---&gt;  java.util.List
 *     object      &lt;---&gt;  java.util.Map
 *     null        &lt;---&gt;  null
 * </pre>
 * 
 * @author Vladimir Dzhuvinov
 */
public abstract class JSONRPC2Message implements JSONAware {


	/**
	 * Map of non-standard JSON-RPC 2.0 message members, {@code null} if
	 * none.
	 */
	private Map <String,Object> nonStdMembers = null;
	

	/** 
	 * Provides common parsing of JSON-RPC 2.0 requests, notifications 
	 * and responses. Use this method if you don't know which type of 
	 * JSON-RPC message the input JSON string represents.
	 *
	 * <p>Batched requests / notifications are not supported.
	 *
	 * <p>This method is thread-safe.
	 *
	 * <p>If you are certain about the message type use the dedicated 
	 * {@link JSONRPC2Request#parse}, {@link JSONRPC2Notification#parse} 
	 * or {@link JSONRPC2Response#parse} methods. They are more efficient 
	 * and provide a more detailed parse error reporting.
	 *
	 * @param jsonString A JSON string representing a JSON-RPC 2.0 request, 
	 *                   notification or response, UTF-8 encoded. Must not
	 *                   be {@code null}.
	 *
	 * @return An instance of {@link JSONRPC2Request}, 
	 *         {@link JSONRPC2Notification} or {@link JSONRPC2Response}.
	 *
	 * @throws JSONRPC2ParseException With detailed message if parsing 
	 *                                failed.
	 */
	public static JSONRPC2Message parse(final String jsonString)
		throws JSONRPC2ParseException {

		return parse(jsonString, false, false);
	}
	
	
	/** 
	 * Provides common parsing of JSON-RPC 2.0 requests, notifications 
	 * and responses. Use this method if you don't know which type of 
	 * JSON-RPC message the input string represents.
	 *
	 * <p>Batched requests / notifications are not supported.
	 *
	 * <p>This method is thread-safe.
	 *
	 * <p>If you are certain about the message type use the dedicated
	 * {@link JSONRPC2Request#parse}, {@link JSONRPC2Notification#parse}
	 * or {@link JSONRPC2Response#parse} methods. They are more efficient
	 * and provide a more detailed parse error reporting.
	 *
	 * @param jsonString A JSON string representing a JSON-RPC 2.0 request,
	 *                   notification or response, UTF-8 encoded. Must not
	 *                   be {@code null}.
	 * @param options    The parse options.
	 *
	 * @return An instance of {@link JSONRPC2Request}, 
	 *         {@link JSONRPC2Notification} or {@link JSONRPC2Response}.
	 *
	 * @throws JSONRPC2ParseException With detailed message if parsing 
	 *                                failed.
	 */
	public static JSONRPC2Message parse(final String jsonString, final JSONRPC2Parser.Option ... options)
		throws JSONRPC2ParseException {
		
		JSONRPC2Parser parser = new JSONRPC2Parser(options);
		return parser.parseJSONRPC2Message(jsonString);
	}


	/**
	 * Provides common parsing of JSON-RPC 2.0 requests, notifications
	 * and responses. Use this method if you don't know which type of
	 * JSON-RPC message the input string represents.
	 *
	 * <p>Batched requests / notifications are not supported.
	 *
	 * <p>This method is thread-safe.
	 *
	 * <p>If you are certain about the message type use the dedicated
	 * {@link JSONRPC2Request#parse}, {@link JSONRPC2Notification#parse}
	 * or {@link JSONRPC2Response#parse} methods. They are more efficient
	 * and provide a more detailed parse error reporting.
	 *
	 * @param jsonString    A JSON string representing a JSON-RPC 2.0
	 *                      request, notification or response, UTF-8
	 *                      encoded. Must not be {@code null}.
	 * @param preserveOrder If {@code true} the member order of JSON
	 *                      objects in parameters and results will be
	 *                      preserved.
	 * @param ignoreVersion If {@code true} the {@code "jsonrpc":"2.0"}
	 *                      version field in the JSON-RPC 2.0 message will
	 *                      not be checked.
	 *
	 * @return An instance of {@link JSONRPC2Request},
	 *         {@link JSONRPC2Notification} or {@link JSONRPC2Response}.
	 *
	 * @throws JSONRPC2ParseException With detailed message if parsing
	 *                                failed.
	 */
	@Deprecated
	public static JSONRPC2Message parse(final String jsonString, final boolean preserveOrder, final boolean ignoreVersion)
		throws JSONRPC2ParseException {

		JSONRPC2Parser parser = new JSONRPC2Parser(preserveOrder, ignoreVersion);
		return parser.parseJSONRPC2Message(jsonString);
	}
	
	
	/**
	 * Appends a non-standard member to this JSON-RPC 2.0 message. This is 
	 * done by adding a new member (key / value pair) to the top level JSON 
	 * object representing the message.
	 *
	 * <p>You may use this method to add meta and debugging members, such 
	 * as the request processing time, to a JSON-RPC 2.0 message.
	 *
	 * @param name  The member name. Must not conflict with the existing
	 *              "method", "id", "params", "result", "error" and
	 *              "jsonrpc" members reserved by the JSON-RPC 2.0
	 *              protocol, else an {@code IllegalArgumentException} will
	 *              be thrown. Must not be {@code null} either.
	 * @param value The member value. Must be of type String, boolean,
	 *              number, List, Map or null, else an
	 *              {@code IllegalArgumentException} will be thrown.
	 */
	public void appendNonStdMember(final String name, final Object value) {
	
		// Name check
		if (name == null          ||
		    name.equals("method") ||
		    name.equals("id")     ||
		    name.equals("params") ||
		    name.equals("result") ||
		    name.equals("error")  ||
		    name.equals("jsonrpc")   )
	
			throw new IllegalArgumentException("Non-standard member name violation");
	
		// Value check
		if ( value != null                &&
		     ! (value instanceof Boolean) &&
		     ! (value instanceof Number)  &&
		     ! (value instanceof String)  &&
		     ! (value instanceof List)    &&
		     ! (value instanceof Map)        )
		     
			throw new IllegalArgumentException("Illegal non-standard member value, must map to a valid JSON type");
		
		
		if (nonStdMembers == null)
			nonStdMembers = new HashMap<String,Object>();
		
		nonStdMembers.put(name, value);
	}
	
	
	/**
	 * Appends a non-standard member to this JSON-RPC 2.0 message. This is 
	 * done by adding a new member (key / value pair) to the top level JSON 
	 * object representing the message.
	 *
	 * <p>You may use this method to add meta and debugging members, such 
	 * as the request processing time, to a JSON-RPC 2.0 message.
	 *
	 * @param name  The member name. Must not conflict with the existing
	 *              "method", "id", "params", "result", "error" and
	 *              "jsonrpc" members reserved by the JSON-RPC 2.0
	 *              protocol, else an {@code IllegalArgumentException} will
	 *              be thrown. Must not be {@code null} either.
	 * @param value The member value. Must be of type String, boolean,
	 *              number, List, Map or null, else an
	 *              {@code IllegalArgumentException} will be thrown.
	 */
	@Deprecated
	public void appendNonStdAttribute(final String name, final Object value) {
	
		appendNonStdMember(name, value);
	}
	
	
	/**
	 * Gets a non-standard JSON-RPC 2.0 message member.
	 *
	 * @param name The name of the non-standard members to retrieve. Must
	 *             not be {@code null}.
	 *
	 * @return The value of the non-standard members (may also be 
	 *         {@code null}), {@code null} if not found.
	 */
	public Object getNonStdMember(final String name) {
	
		if (nonStdMembers == null)
			return null;
		
		return nonStdMembers.get(name);
	}
	
	
	/**
	 * Gets a non-standard JSON-RPC 2.0 message member.
	 *
	 * @param name The name of the non-standard members to retrieve. Must
	 *             not be {@code null}.
	 *
	 * @return The value of the non-standard members (may also be 
	 *         {@code null}), {@code null} if not found.
	 */
	@Deprecated
	public Object getNonStdAttribute(final String name) {
	
		return getNonStdMember(name);
	}
	
	
	/**
	 * Gets the non-standard JSON-RPC 2.0 message members.
	 *
	 * @return The non-standard members as a map, empty map if none.
	 */
	public Map<String,Object> getNonStdMembers() {
	
		return nonStdMembers != null ? nonStdMembers : Collections.<String, Object>emptyMap();
	}
	
	
	/**
	 * Gets the non-standard JSON-RPC 2.0 message members.
	 *
	 * @return The non-standard members as a map, {@code null} if none.
	 */
	@Deprecated
	public Map<String,Object> getNonStdAttributes() {
	
		return getNonStdMembers();
	}
	
	
	/** 
	 * Returns a JSON object representing this JSON-RPC 2.0 message.
	 *
	 * @return The JSON object.
	 */
	public abstract JSONObject toJSONObject();
	
	
	/**
	 * Returns a JSON string representation of this JSON-RPC 2.0 message.
	 *
	 * @see #toString
	 *
	 * @return The JSON object string representing this JSON-RPC 2.0 
	 *         message.
	 */
	public String toJSONString() {
	
		return toString();
	}
	
	
	/** 
	 * Serialises this JSON-RPC 2.0 message to a JSON object string.
	 *
	 * @return The JSON object string representing this JSON-RPC 2.0 
	 *         message.
	 */
	@Override
	public String toString() {
		
		return toJSONObject().toString();
	}
}
