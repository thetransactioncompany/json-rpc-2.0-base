package com.thetransactioncompany.jsonrpc2;


import java.util.Map;

import junit.framework.TestCase;

import net.minidev.json.JSONAware;
import net.minidev.json.JSONObject;


/**
 * Tests the JSON-RPC 2.0 response class.
 */
public class JSONRPC2ResponseTest extends TestCase {


	public class Bean {


		protected String test = "foo";


		public String getTest() {
			return this.test;
		}
	}


	public void testSetIDFromToString() {

		Object id = new Object() {
			@Override
			public String toString() {
				return "id";
			}
		};

		JSONRPC2Response response = new JSONRPC2Response(id);

		assertEquals("id", response.getID());
	}


	public void testSetResultJsonAware()
		throws JSONRPC2ParseException {

		JSONRPC2Parser parser = new JSONRPC2Parser();

		JSONAware result = new JSONAware() {
			public String toJSONString() {
				return "\"dummy\"";
			}
		};

		JSONRPC2Response response = new JSONRPC2Response(result, "id");
		// we need to check serialized form
		response = parser.parseJSONRPC2Response(response.toJSONString());

		assertEquals("dummy", response.getResult());
	}


	public void testSetResultBean()
		throws JSONRPC2ParseException {

		JSONRPC2Parser parser = new JSONRPC2Parser();

		Object result = new JSONRPC2ResponseTest.Bean();

		JSONRPC2Response response = new JSONRPC2Response(result, "id");
		// we need to check serialized form
		response = parser.parseJSONRPC2Response(response.toJSONString());

		result = response.getResult();
		assertTrue(result instanceof Map);
		assertTrue(((Map) result).containsKey("test"));
		assertEquals("foo", ((Map) result).get("test"));
	}


	public void testParse_nullError() {

		String result = "result-123";
		String id = "id-123";

		JSONRPC2Response response = new JSONRPC2Response(result, id);

		JSONObject jsonObject = response.toJSONObject();
		jsonObject.put("error", null);

		try {
			JSONRPC2Response.parse(jsonObject.toJSONString());
			fail();
		} catch (JSONRPC2ParseException e) {
			assertEquals("Invalid JSON-RPC 2.0 response: Must not include both result and error", e.getMessage());
		}
	}


	public void testParse_allowNullError()
		throws JSONRPC2ParseException {

		String result = "result-123";
		String id = "id-123";

		JSONRPC2Response response = new JSONRPC2Response(result, id);

		JSONObject jsonObject = response.toJSONObject();
		jsonObject.put("error", null);

		response = JSONRPC2Response.parse(jsonObject.toJSONString(), JSONRPC2Parser.Option.ALLOW_NULL_ERROR_IN_RESPONSE);

		assertEquals(result, response.getResult());
		assertEquals(id, response.getID());
	}
}
