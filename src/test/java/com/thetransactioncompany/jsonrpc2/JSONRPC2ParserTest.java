package com.thetransactioncompany.jsonrpc2;


import com.thetransactioncompany.jsonrpc2.util.NamedParamsRetriever;
import junit.framework.TestCase;

import java.util.Map;


/**
 * Tests the JSON-RPC 2.0 message parser.
 *
 * @author Vladimir Dzhuvinov
 */
public class JSONRPC2ParserTest extends TestCase {


	public void testConstructorWithOptions() {

		JSONRPC2Parser parser = new JSONRPC2Parser(JSONRPC2Parser.Option.IGNORE_VERSION, JSONRPC2Parser.Option.ALLOW_NULL_ERROR_IN_RESPONSE);

		assertTrue(parser.getOptions().contains(JSONRPC2Parser.Option.IGNORE_VERSION));
		assertTrue(parser.getOptions().contains(JSONRPC2Parser.Option.ALLOW_NULL_ERROR_IN_RESPONSE));
		assertEquals(2, parser.getOptions().size());

		assertTrue(parser.ignoresVersion());

		parser.ignoreVersion(false);

		assertTrue(parser.getOptions().contains(JSONRPC2Parser.Option.ALLOW_NULL_ERROR_IN_RESPONSE));
		assertEquals(1, parser.getOptions().size());

		assertFalse(parser.ignoresVersion());
	}


	public void testParseRequest()
		throws Exception {

		String json = "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42, 23], \"id\": 1}";

		JSONRPC2Parser parser = new JSONRPC2Parser();
		assertTrue(parser.getOptions().isEmpty());

		JSONRPC2Request request = parser.parseJSONRPC2Request(json);
		assertEquals("subtract", request.getMethod());
		assertEquals(JSONRPC2ParamsType.ARRAY, request.getParamsType());
		assertEquals(42L, request.getPositionalParams().get(0));
		assertEquals(23L, request.getPositionalParams().get(1));
		assertEquals(2, request.getPositionalParams().size());
		assertEquals(1L, request.getID());
	}


	public void testParseRequestWithTrailingWhiteSpace()
		throws Exception {

		String json = "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42, 23], \"id\": 1}   ";

		JSONRPC2Parser parser = new JSONRPC2Parser();
		assertTrue(parser.getOptions().isEmpty());

		JSONRPC2Request request = parser.parseJSONRPC2Request(json);
		assertEquals("subtract", request.getMethod());
		assertEquals(JSONRPC2ParamsType.ARRAY, request.getParamsType());
		assertEquals(42L, request.getPositionalParams().get(0));
		assertEquals(23L, request.getPositionalParams().get(1));
		assertEquals(2, request.getPositionalParams().size());
		assertEquals(1L, request.getID());
	}


	public void testParseCatchesNumberFormatException() {

		String json = "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [2e+], \"id\": 1}   ";

		try {
			new JSONRPC2Parser().parseJSONRPC2Request(json);
			fail();
		} catch (JSONRPC2ParseException e) {
			assertEquals("Invalid JSON", e.getMessage());
		}
	}


	public void testParseResponseWithNullError() {
		
		String json =
			"{" +
			"  \"result\": {" +
			"    \"valid\": true" +
			"  }," +
			"  \"error\": null," +
			"  \"id\": \"id\"" +
			"}";

		try {
			new JSONRPC2Parser(JSONRPC2Parser.Option.IGNORE_VERSION).parseJSONRPC2Response(json);
			fail();
		} catch (JSONRPC2ParseException e) {
			assertEquals("Invalid JSON-RPC 2.0 response: Must not include both result and error", e.getMessage());
		}
	}


	public void testParseResponseWithNullError_allowNullError()
		throws JSONRPC2ParseException, JSONRPC2Error {

		String json =
			"{" +
			"  \"result\": {" +
			"    \"valid\": true" +
			"  }," +
			"  \"error\": null," +
			"  \"id\": \"id-001\"" +
			"}";

		JSONRPC2Response response = new JSONRPC2Parser(
			JSONRPC2Parser.Option.IGNORE_VERSION,
			JSONRPC2Parser.Option.ALLOW_NULL_ERROR_IN_RESPONSE)
			.parseJSONRPC2Response(json);


		assertTrue(new NamedParamsRetriever((Map<String, Object>)response.getResult()).getBoolean("valid"));
		assertEquals("id-001", response.getID());
		assertTrue(response.getNonStdMembers().isEmpty());
	}


	public void testParseResponse_noResult_noError() {

		String json =
			"{" +
			"  \"id\": \"id-001\"," +
			"  \"jsonrpc\": \"2.0\"" +
			"}";

		try {
			new JSONRPC2Parser().parseJSONRPC2Response(json);
			fail();
		} catch (JSONRPC2ParseException e) {
			assertEquals("Invalid JSON-RPC 2.0 response: Neither result nor error specified", e.getMessage());
		}
	}
}
