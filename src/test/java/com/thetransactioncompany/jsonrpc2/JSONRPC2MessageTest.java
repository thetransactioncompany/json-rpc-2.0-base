package com.thetransactioncompany.jsonrpc2;

import junit.framework.TestCase;

/**
 * @author robertzieschang 02.02.24
 **/
public class JSONRPC2MessageTest extends TestCase {

    public void testParseMessage_isRequest() throws JSONRPC2ParseException {
        String json = "{,\"jsonrpc\":\"2.0\", \"method\":\"progressNotify\",\"params\":[\"75%\"], \"id\": 1}";
        JSONRPC2Message message = JSONRPC2Message.parse(json);
        assertTrue(message instanceof JSONRPC2Request);
    }

    public void testParseMessage_isResponse_withMethodParam() throws JSONRPC2ParseException {
        String json = "{\"jsonrpc\": \"2.0\", \"method\": \"Init\", \"result\": [42, 23], \"id\": 1}";
        JSONRPC2Message message = JSONRPC2Message.parse(json);
        assertTrue(message instanceof JSONRPC2Response);
    }

    public void testParseMessage_isNotification() throws JSONRPC2ParseException {
        String json = "{\"jsonrpc\": \"2.0\", \"method\": \"Init\", \"params\": [42, 23] }";
        JSONRPC2Message message = JSONRPC2Message.parse(json);
        assertTrue(message instanceof JSONRPC2Notification);
    }
}
