package com.thetransactioncompany.jsonrpc2;

import junit.framework.TestCase;

import static org.junit.Assert.assertNotEquals;

public class JSONRPC2ErrorTest extends TestCase {


        public void testConstants() {

                assertEquals(-32700, JSONRPC2Error.PARSE_ERROR.getCode());
                assertEquals("JSON parse error", JSONRPC2Error.PARSE_ERROR.getMessage());

                assertEquals(-32600, JSONRPC2Error.INVALID_REQUEST.getCode());
                assertEquals("Invalid request", JSONRPC2Error.INVALID_REQUEST.getMessage());

                assertEquals(-32601, JSONRPC2Error.METHOD_NOT_FOUND.getCode());
                assertEquals("Method not found", JSONRPC2Error.METHOD_NOT_FOUND.getMessage());

                assertEquals(-32602, JSONRPC2Error.INVALID_PARAMS.getCode());
                assertEquals("Invalid parameters", JSONRPC2Error.INVALID_PARAMS.getMessage());

                assertEquals(-32603, JSONRPC2Error.INTERNAL_ERROR.getCode());
                assertEquals("Internal error", JSONRPC2Error.INTERNAL_ERROR.getMessage());
        }


        public void testEqualityAndHashCode_isCodeBase() {

                assertEquals(new JSONRPC2Error(-32700, "msg"), JSONRPC2Error.PARSE_ERROR);
                assertEquals(new JSONRPC2Error(-32700, "msg").hashCode(), JSONRPC2Error.PARSE_ERROR.hashCode());
        }


        public void testInequalityAndHashCode() {

                assertNotEquals(new JSONRPC2Error(1, "msg"), new JSONRPC2Error(2, "msg"));
                assertNotEquals(new JSONRPC2Error(1, "msg").hashCode(), new JSONRPC2Error(2, "msg").hashCode());
        }


        public void testRequireNonNullMessage() {

                try {
                        new JSONRPC2Error(100, null);
                        fail();
                } catch (NullPointerException e) {
                        assertNull(e.getMessage());
                }
        }


        public void testRequireNonNullMessage_withDataArgument() {

                try {
                        new JSONRPC2Error(100, null, null);
                        fail();
                } catch (NullPointerException e) {
                        assertNull(e.getMessage());
                }
        }
}
