# JSON-RPC 2.0 Base

Copyright (c) Vladimir Dzhuvinov, 2009 - 2024

Java classes to represent, parse and serialise JSON-RPC 2.0 requests, 
notifications and responses.

Requirements:

* Java 1.8 or later

* The package depends on the JSON Smart library for JSON encoding and decoding 
  (fork of the popular JSON.simple toolkit, but with more efficient parsing).

Visit the library home page for usage, examples and updates:

<https://software.dzhuvinov.com/json-rpc-2.0-base.html>
